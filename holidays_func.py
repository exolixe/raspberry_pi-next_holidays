#!/usr/bin/python3

import requests
from datetime import datetime as dt

def get_public_data():
    my_year = dt.now().year
    my_month = dt.now().month
    if my_month < 9:
        scholar_year = "{}-{}".format(my_year - 1, my_year)
    else:
        scholar_year = "{}-{}".format(my_year, my_year + 1)
    url="https://data.education.gouv.fr/api/records/1.0/search/"
    params= { 'dataset': 'fr-en-calendrier-scolaire',
            'refine.annee_scolaire': scholar_year,
            'refine.zones': 'Zone B',
            'refine.location': 'Normandie',
            'timezone': 'Europe/Paris',
            'exclude.population': 'Élèves'
    }

    response = requests.get(url, params=params)

    if not response:
        exit('Erreur de la requete')

    return(response.json())

def init_local_data(data, my_records):
    for record in data['records']:
        my_record = {}
        my_record.update({'lastupdate': record['record_timestamp'][:16]})
        my_record.update({'start_date': record['fields']['start_date'][:16]})
        my_record.update({'end_date': record['fields']['end_date'][:16]})
        description = record['fields']['description']

        if description not in my_records:
            my_records.update({description: my_record})
        else:
            update = dt.strptime(record['record_timestamp'][:16], "%Y-%m-%dT%H:%M")
            my_update = dt.strptime(my_records.get(description).get('lastupdate'), "%Y-%m-%dT%H:%M")
            if update > my_update:
                my_records.update({description: my_record})
    return(my_records)

def get_holidays_start(data):
    curdate = dt.now()
    next_date = None
    next_name = None
    for (holiday, dates) in data.items():
        date_to_check = dt.strptime(dates.get('start_date'), "%Y-%m-%dT%H:%M")
        if next_date is None:
            next_date = date_to_check
            next_name = holiday
        else:
            diff1 = next_date - curdate
            diff2 = date_to_check - curdate
            if diff2.days >= 0 and abs(diff2.days) < abs(diff1.days):
                next_date = date_to_check
                next_name = holiday
    return((next_name, next_date))

def get_holidays_end(data):
    curdate = dt.now()
    next_date = None
    curname = None
    for (holiday, dates) in data.items():
        date_to_check = dt.strptime(dates.get('end_date'), "%Y-%m-%dT%H:%M")
        if next_date is None:
            next_date = date_to_check
            next_name = holiday
        else:
            diff1 = next_date - curdate
            diff2 = date_to_check - curdate
            if diff2.days >= 0 and abs(diff2.days) < abs(diff1.days):
                next_date = date_to_check
                next_name = holiday
    return((next_name, next_date))

def get_next_date(data):
    curdate = dt.now()
    start_name, start_date = get_holidays_start(data)
    end_name, end_date = get_holidays_end(data)

    if start_date <= end_date:
        time_left = start_date - curdate
        return((start_name, "Début le " + start_date.strftime("%d/%m/%Y"), "Plus que {} jour(s) d'attente !".format(time_left.days)))
    else:
        time_left = end_date - curdate
        return((start_name, "Fin le " + end_date.strftime("%d/%m/%Y"), "Encore {} jour(s) de vacances !".format(time_left.days)))
