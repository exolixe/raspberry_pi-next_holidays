# Raspberry next_holidays

## Hardware
* raspberry pi zero WH https://www.kubii.fr/cartes-raspberry-pi/2076-raspberry-pi-zero-wh-kubii-3272496009394.html
* waveshare e-paper 2.13 https://www.waveshare.com/product/displays/e-paper/epaper-3/2.13inch-e-paper-hat.htm


## Software
* France's National Education API https://data.education.gouv.fr/api/v1/console/datasets/1.0/search/

## TODO
- comment the code
- review the code
- update to educnat API v2