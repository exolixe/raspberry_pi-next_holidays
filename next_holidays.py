#!/usr/bin/python3

import logging
from waveshare_epd import epd2in13_V2
from PIL import Image,ImageDraw,ImageFont

import time
from holidays_func import get_public_data, init_local_data, get_next_date

# Initialisation de variables
my_records = {}
wait_before_check = 3600
wait_before_data_dl = 0
hname, hdate = None, None

# Wait for network to be up
time.sleep(30)

# Loop to print date on screen
try:
    epd = epd2in13_V2.EPD()
    epd.init(epd.FULL_UPDATE)
    epd.Clear(0xFF)
    font = ImageFont.truetype('/home/pi/api_vacances/Font.ttc', 15)
    info_image = Image.new('1', (epd.height, epd.width), 255)
    info_draw = ImageDraw.Draw(info_image)

    epd.displayPartBaseImage(epd.getbuffer(info_image))

    epd.init(epd.PART_UPDATE)
    while True:
        if wait_before_data_dl <= 0:
            try: 
                data = get_public_data()
            except:
                print("Attempt to access API failed. Retrying ...")
            my_records = init_local_data(data, my_records)
            wait_before_data_dl = 24 * 3600 # Wait 1 day before update
        pname, pdate, ptime = get_next_date(my_records)

        # Print only if dates are different
        info_draw.rectangle((0, 0, 220, 105), fill = 255)
        if hdate is None or hdate != pdate:
            hdate, hname, htime = pdate, pname, ptime
#            print(pname)
#            print(pdate)
#            print(ptime)
        info_draw.text((0, 0), pname, font = font, fill = 0)
        info_draw.text((0, 30), pdate, font = font, fill = 0)
        info_draw.text((0, 60), ptime, font = font, fill = 0)
        epd.displayPartial(epd.getbuffer(info_image))

    time.sleep(wait_before_check)
    wait_before_data_dl -= wait_before_check

except IOError as e:
    logging.info(e)

except KeyboardInterrupt:    
    logging.info("ctrl + c:")
    epd.init(epd.FULL_UPDATE)
    epd.sleep()
    epd2in13_V2.epdconfig.module_exit()
    exit()
